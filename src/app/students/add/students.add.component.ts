import { Component } from '@angular/core';
import Student from '../../entity/student';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { StudentService } from 'src/app/service/student-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})
export class StudentsAddComponent {
  ngOnInit() : void {
    this.form = this.fb.group({
      studentId: [null, Validators.compose(
        [Validators.required, 
        Validators.maxLength(13)])],
      name: [null, Validators.required],
      surname: [null, Validators.required],
      penAmount: [null, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]+')])
      ],
      image: [null],
      description: [null]
    });
  }
  students: Student[];
  form = this.fb.group({
    id:[''],
    studentId: [''],
    name: [''],
    surname: [''],
    gpa: [''],
    image: [''],
    featured: [''],
    penAmount: [''],
    description:['']
  });
  
  validation_messages = {
    'studentId': [
      { type: 'required', message: 'student id is required'},
      { type: 'maxlength', message: 'student id is too long'}
    ],
    'name': [
      {type: 'required', message: 'the name is required'}
    ],
    'surname': [
      { type: 'required', message: 'the surname is required'}
    ],
    'penAmount': [
      { type: 'required', message: 'the penAmount is required'},
      { type: 'pattern', message: 'please enter number'}
    ],
    'image': [],
    'description': []

  };
  
  get diagnostic() {    
     return JSON.stringify(this.form.value);}; 

  upQuantity(student: Student) {            
    this.form.patchValue({
      penAmount:+this.form.value['penAmount']+1});
  }

  downQuantity(student: Student) {
    if (+this.form.value['penAmount'] > 0) {
      this.form.patchValue({
        penAmount:+this.form.value['penAmount']-1});
    }
  }

  submit(){
      this.studentService.saveStudent(this.form.value)
      .subscribe((student) => {
          this.router.navigate(['/detail/', student.id]);
      }, (error) => {
        alert('could not save value');
      });
  }

  
  constructor(private fb: FormBuilder, private studentService:StudentService,private router: Router) { 

  }
}
